
/**
 * Initialize settings.
 */
Drupal.settings.asynchronous = Drupal.settings.asynchronous || {};

/**
 * Drupal behavior.
 */
Drupal.behaviors.asynchronous = function(context) {
  var settings = $.extend({
    basePath: Drupal.settings.basePath
  }, Drupal.settings.asynchronous);

  var asynchronous = Drupal.asynchronous.getInstance(context, settings);
  asynchronous.update(context);
};

/**
 * Singleton instance.
 */
Drupal.asynchronousInstance = null;

/**
 * Constructor.
 */
Drupal.asynchronous = function(context, settings) {
  this.initialized = false;
  this.base = true;

  this.settings = $.extend({
    cleanURLs: false,
    basePath: '/',
    preprocess: null,
    selectors: {}
  }, settings);
};

/**
 * Pseudo singleton.
 */
Drupal.asynchronous.getInstance = function(context, settings) {
  // Handle the singleton stuff.
  if (!Drupal.asynchronousInstance) {
    Drupal.asynchronousInstance = new Drupal.asynchronous(context, settings);
  }
  return Drupal.asynchronousInstance;
};

/**
 * Implementations.
 */
Drupal.asynchronous.prototype = {

  /**
   * Init.
   */
  init: function(context) {

    // Loading...
    var loading = $('<div id="loading">Loading...</div>')
      .bind('ajaxSend', function() {
        $(this).show();
      })
      .bind('ajaxComplete', function() {
        $(this).hide();
      })
      .appendTo($('body'));
  
    var self = this;
  
    var historyCallback = function(href) {
      self.loadContent(href);
    };
  
    // History and bookmarking support.
    $.history.init(historyCallback);
  },

  update: function(context) {
    if (!this.initialized) {
      this.init(context);
      this.initialized = true;
    }
    this.processLinks(context);
    this.processForms(context);
  },

  /**
   * Attach behaviors to every <a>.
   */
  processLinks: function(context) {
    var self = this;
    $('a:not(.asynchronous-processed):not(.views-ajax-link)', context).each(function() {
      // Check if this is an actual link and if this is not an external one.
      if (this.href && this.hostname == location.hostname) {
        // Construct a nice URL.
        var url = this.pathname + this.search.replace(/\?q=/, '') + this.hash;

        // Some browsers don't include leading "/" on pathname, so we include
        // it here for compatibility.
        if (this.pathname.length > 1 && this.pathname.substr(0, 1) != '/') {
          url = '/' + url;
        }

        // Remove base path from URL.
        url = url.substr(self.settings.basePath.length) || '';

        // If href is null we are about to go to the homepage.
        this.href = '#' + url;
  
        $(this).click(function() {
          $.history.load(url);
          return false;
        });

        $(this).addClass('asynchronous-processed');
      }
    });
  },

  /**
   * Attach behaviors to every <form>.
   */
  processForms: function(context) {
    var self = this;
    var options = {
      dataType: 'json',
      success: function(data) {
        self.processData(data);
        // Scroll to the top of the page.
        self.scrollTo(false);
      }
    };
    $('form', context).ajaxForm(options);
  },

  /**
   * Ajax magic.
   */
  loadContent: function(url) {
    // If there is a hash in the url, get it so we can scroll to the target element.
    if (url.indexOf('#') != -1) {
      var parts = url.split('#');
      url = parts[0];
      var hash = parts[1];
    }

    // Check if we are on the base page. If so, do nothing and return.
    if (!url && this.base) {
      this.base = false;
      return false;
    }

    var self = this;
    $.ajax({
      url: '?q=' + url,
      dataType: 'json',
      data: {page_renderer: 'json'},
      success: function(data) {
        self.processData(data, url);
        self.scrollTo(hash || false);
      },
      error: function(xhr, textStatus, errorThrown) {
        if(xhr.responseText) {
          // The response error content (i.e. access denied) should also be json
          // but jQuery returns it as string so we convert it.
          var data = eval('(' + xhr.responseText + ')');
          self.processData(data, url);
          self.scrollTo(hash || false);
        }
        else {
          // TODO: Move this to a better error reporting tool, like gmail one
          // that display the error message in the "Loading..." box and auto
          // retry in few seconds, also with a link to "retry now".
          alert('An error occured. Please try to refresh the page.');
        }
      }
    });
  },

  processData: function(data, url) {
    var self = this;
    if (self.settings.preprocess) {
      self.settings.preprocess(data, url);
    }
    var selectors = self.settings.selectors;
    var newContent = jQuery();
    $.each(selectors, function(i, val) {
      if (data[i]) {
        var elm = $(selectors[i].selector);
        elm[selectors[i].type](data[i]);
        newContent.add(elm);
      }
    });

    // Add scripts.
    // Load only scripts which aren't in the current DOM. Trying to put them
    // in the expected order (TODO: this seems not actually necessary).
    // This will also, hopefully, add inline scripts.
    $(data.scripts).filter('script').each(function() {
      var src = $(this).attr('src');
      // Handle inline javascript.
      if (!src) {
        $('head').append(this);
      }
      else if (!$('script[src="'+ src +'"]').get(0)) {
        var prevSrc = $(this).prev().attr('src');
        $('script[src="'+ prevSrc +'"]').after(this);
      }
    });
    
    // Add stylesheets.
    $(data.styles).filter('link[type=text/css][rel=stylesheet]').each(function() {
      var href = $(this).attr('href');
      if (!$('link[href="'+ href +'"]').get(0)) {
        var prevHref = $(this).prev().attr('href');
        $('link[href="'+ prevHref +'"]').after(this);
      }
    });

    // Update page title.
    document.title = data.head_title;

    // Attach behaviors to new loaded content.
    Drupal.attachBehaviors(newContent);
  },

  scrollTo: function(target) {
    // Default to the top of page.
    var offsetTop = 0; 

    if (target) {
      var $target = $('#' + target) || $('[name=' + target +']') || null;
      if ($target) {
        offsetTop = $target.offset().top;
      }
    }

    $('html, body').animate({
      scrollTop: offsetTop
    }, 'medium');

  }

};

/**
 * Redirect non dynamic URLs to dynamic ones.
 */
Drupal.asynchronous.checkLocation = function() {
  var path = location.pathname + location.search.replace(/\?q=/, '');

  // Remove base path.
  path = path.substr(Drupal.settings.basePath.length) || '';

  // Remove leading slash if present.
  path = (path.substr(0, 1) == '/') ? path.substring(1) : path;
  //alert(path);

  if (path != '') {
    location = Drupal.settings.basePath + '#' + path + location.hash;
  }
}
